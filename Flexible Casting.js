//Macro for Sorcerer Flexible Casting DnD 5e
const ActorUser = game.actors.get(args[0].actor._id)
console.log(ActorUser)
let spells = []
let SP = ActorUser.data.data.resources.primary.value
let num1 = SP
for (let i = 1;i<10;i++){
    eval('spells[' + i + '] = ActorUser.data.data.spells.spell' + i + '.value;')
}
let d = new Dialog({
  title: 'Flexible Casting',
  buttons: {
    Melee: {
      label: 'Create Spell Slots',
      callback: (html) => CriaSpell()
    },
    Spell: {
      label: 'Create Sorcery Points',
      callback: (html) => CriaSP()
    },
  }
}).render(true);

async function CriaSpell(){
    let slots = '';
    for(let i = 1; i < SP && i < 6;i++){
            slots = slots + '<option value=' + i + '> Level ' + i + ' (' + spells[i] + ' Slots)</option>'}
    let opt = '<div class="form-group"><label for="exampleSelect">Select spell slot</label><select name="exampleSelect">' + slots + '</select></div>'
    
    let d = await new Dialog({
        title: 'Create Spell Slots',
        content: opt,
        buttons: {
          yes: {
            icon: '<i class="fas fa-check"></i>',
            label: 'OK',
            callback: (html) => {
              let select = html.find('[name="exampleSelect"]').val();          
              SP = Number(SP) - Number(select) - 1
              eval('ActorUser.update({\'data.spells.spell' + select + '.value\':ActorUser.data.data.spells.spell' + select + '.value + 1})')
              if(SP>ActorUser.data.data.resources.primary.max){
                ActorUser.update({'data.resources.primary.value': ActorUser.data.data.resources.primary.max})}
              else{
                  ActorUser.update({'data.resources.primary.value': SP})
                }
            },
        },
    }}
    ).render(true)
}

async function CriaSP(){
    let slots = '';
    for(let i = 1; i < spells.length;i++){
        if(spells[i]>0){
            slots = slots + '<option value=' + i + '> Level ' + i + ' (' + spells[i] + ' Slots)</option>'
        }
    }
    let opt = '<div class="form-group"><label for="exampleSelect">Select spell slot</label><select name="exampleSelect">' + slots + '</select></div>'

let d = await new Dialog({
    title: 'Create sorcery points',
    content: opt,
    buttons: {
      yes: {
        icon: '<i class="fas fa-check"></i>',
        label: 'OK',
        callback: (html) => {
          let select = html.find('[name="exampleSelect"]').val();          
          SP = Number(SP) + Number(select)
          eval('ActorUser.update({\'data.spells.spell' + select + '.value\':ActorUser.data.data.spells.spell' + select + '.value - 1})')
          if(SP>ActorUser.data.data.resources.primary.max){
            ActorUser.update({'data.resources.primary.value': ActorUser.data.data.resources.primary.max})}
          else{ActorUser.update({'data.resources.primary.value': SP})}
        },
    },
}}
).render(true)
}
